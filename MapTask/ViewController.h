//
//  ViewController.h
//  MapTask
//
//  Created by А on 13.08.2018.
//  Copyright © 2018 А. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController : UIViewController <GMSMapViewDelegate>

@end

