#import "LocationManager.h"

@interface LocationManager() {
    CLLocationManager *locationManager;
    void (^exeptingBlock)(CLLocation*);
}
@end

static LocationManager *instance;

@implementation LocationManager

+(LocationManager*)getLocationManager {
    if([CLLocationManager locationServicesEnabled]){
        if (!instance) {
            instance = [[LocationManager alloc] init];
            instance->locationManager = [[CLLocationManager alloc] init];
            instance->locationManager.delegate = instance;
            instance->locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            instance->locationManager.distanceFilter = 100;
        }
        return instance;
    }
    return nil;
}

-(void)performBlock:(void (^)(CLLocation*))block {
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    self->exeptingBlock = block;
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegatMethods

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"error - %@\n", error.localizedFailureReason);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [manager stopUpdatingLocation];
    NSLog(@"Location - %@\n", locations);
    CLLocation *location = [locations lastObject];
    if (self->exeptingBlock) {
        exeptingBlock(location);
    }
}

@end
