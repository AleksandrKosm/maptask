#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate>

+(LocationManager*)getLocationManager;
-(void)performBlock:(void (^)(CLLocation*))block;

@end
