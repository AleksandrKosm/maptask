//
//  AddresManager.h
//  MapTask
//
//  Created by А on 14.08.2018.
//  Copyright © 2018 А. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AddresManager : NSObject

+(void)getAddressBy:(CLLocationCoordinate2D)location complition:(void (^)(NSString* address))block;

@end
