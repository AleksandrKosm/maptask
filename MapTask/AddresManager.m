//
//  AddresManager.m
//  MapTask
//
//  Created by А on 14.08.2018.
//  Copyright © 2018 А. All rights reserved.
//

#import "AddresManager.h"
#import <GooglePlaces/GooglePlaces.h>
#import <GoogleMaps/GoogleMaps.h>

@implementation AddresManager

+(void)getAddressBy:(CLLocationCoordinate2D)location complition:(void (^)(NSString* address))block {
    GMSGeocoder *geocoder = [[GMSGeocoder alloc] init];
    [geocoder reverseGeocodeCoordinate:location completionHandler:^(GMSReverseGeocodeResponse *result, NSError *error) {
        if (error) {
            NSLog(@"Error %@", error.description);
            block(nil);
        } else {
            GMSAddress* address = [result firstResult];
            NSString *str = [[NSString alloc] init];
            for (NSString *tmp in address.lines) {
                if (str.length > 0) {
                    str = [str stringByAppendingString:@", "];
                }
                str = [str stringByAppendingString:tmp];
            }
            block(str);
        }
    }];
}

@end
