//
//  ViewController.m
//  MapTask
//
//  Created by А on 13.08.2018.
//  Copyright © 2018 А. All rights reserved.
//

#import "ViewController.h"
#import "LocationManager.h"
#import "AddresManager.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet GMSMapView *MapView;
@property (weak, nonatomic) IBOutlet UIButton *userLocationBtn;
@property (weak, nonatomic) IBOutlet UILabel *addressLable;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib
    _userLocationBtn.layer.cornerRadius = _userLocationBtn.frame.size.height/2;
    [self.view viewWithTag:1].layer.cornerRadius = 15;
    _MapView.delegate = self;
    [self moveToUserLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)userLocationButton:(id)sender {
    [self moveToUserLocation];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    [AddresManager getAddressBy:position.target complition:^(NSString *address) {
        if (address) {
            NSDictionary *attributes = @{NSFontAttributeName: [self->_addressLable font]};
            CGRect rect = [address boundingRectWithSize:CGSizeMake(self->_addressLable.frame.size.width, CGFLOAT_MAX)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:attributes
                                                      context:nil];
            UIView *tmp = [self.view viewWithTag:1];
            rect = CGRectMake(tmp.frame.origin.x,
                              tmp.frame.origin.y - rect.size.height + self->_addressLable.frame.size.height,
                              tmp.frame.size.width,
                              tmp.frame.size.height + rect.size.height - self->_addressLable.frame.size.height);
            [self->_addressLable setAlpha:0];
            [self->_addressLable setText:address];
            [UIView animateWithDuration:0.3 animations:^{
                [tmp setFrame:rect];
                [self->_addressLable setAlpha:1];
            }];
        }
    }];
}

- (void)moveToUserLocation {
    LocationManager *manager = [LocationManager getLocationManager];
    if (manager) {
        [manager performBlock:^(CLLocation* location){
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                    longitude:location.coordinate.longitude
                                                                         zoom:self->_MapView.camera.zoom];
            [self->_MapView animateToCameraPosition:camera];
        }];
    }
}

@end
